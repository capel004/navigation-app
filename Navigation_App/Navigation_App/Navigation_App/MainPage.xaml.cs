﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace Navigation_App
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

      

        private async void Handeler_Page_2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SecondPage(text_entry.Text));

        }
        private async void Handeler_Page_3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ThirdPage());
        }
    
    }
}
